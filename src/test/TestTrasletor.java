package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import trasletor.Translator;

class TestTrasletor {

	@Test
	public void testGetSentence() {
		String sentence = "hello Word"; 
		Translator t = new Translator (sentence);
		assertEquals(sentence,t.getSentence());	
	}
	
	@Test
	public void testTraslateEmpySentence() {
		String sentence = "";
		Translator t = new Translator(sentence);
		assertEquals("nil",t.translate());	
		
	}

	@Test
	public void test3Vowel() {
		String sentence = "array";
		Translator t = new Translator(sentence);
		assertEquals(sentence+"nay",t.translate());	


	}
	
}
